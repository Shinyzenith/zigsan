const std = @import("std");
const UbsanTypes = @import("ubsan/types.zig");

comptime {
    makeHandler("handle_add_overflow", "Addition", "+");
    makeHandler("handle_sub_overflow", "Subtraction", "-");
}

fn exportHandlers(handlers: anytype, comptime export_name: []const u8) void {
    const linkage: std.builtin.GlobalLinkage = .Weak;

    {
        const handler_symbol = std.builtin.ExportOptions{
            .name = "__ubsan_" ++ export_name,
            .linkage = linkage,
        };
        @export(handlers.recover_handler, handler_symbol);
    }

    {
        const handler_symbol = std.builtin.ExportOptions{
            .name = "__ubsan_" ++ export_name ++ "_abort",
            .linkage = linkage,
        };
        @export(handlers.abort_handler, handler_symbol);
    }
}

fn handleOverflow(overflow_data: *UbsanTypes.OverflowData, comptime name: []const u8, comptime symbol: []const u8, lhs: UbsanTypes.ValueHandle, rhs: UbsanTypes.ValueHandle) void {
    const source_location = overflow_data.source_location.acquire();
    const format_string = name ++ " Overflow: {} " ++ symbol ++ " {} in type {s}\n" ++ "{s}:{}:{}\n";
    const type_name = overflow_data.type_descriptor.getNameAsString();

    if (overflow_data.type_descriptor.isSignedInteger()) {
        const lhs_value = overflow_data.type_descriptor.getSignedIntValue(lhs);
        const rhs_value = overflow_data.type_descriptor.getSignedIntValue(rhs);
        std.debug.print(format_string, .{ lhs_value, rhs_value, type_name, source_location.file_name orelse "", source_location.line, source_location.column });
    } else {
        const lhs_value = overflow_data.type_descriptor.getUnsignedIntValue(lhs);
        const rhs_value = overflow_data.type_descriptor.getUnsignedIntValue(rhs);
        std.debug.print(format_string, .{ lhs_value, rhs_value, type_name, source_location.file_name orelse "", source_location.line, source_location.column });
    }
}

fn makeHandler(export_name: []const u8, name: []const u8, symbol: []const u8) void {
    const handlers = struct {
        pub fn recover_handler(overflow_data: *UbsanTypes.OverflowData, lhs: UbsanTypes.ValueHandle, rhs: UbsanTypes.ValueHandle) callconv(.C) void {
            handleOverflow(overflow_data, name, symbol, lhs, rhs);
        }

        pub fn abort_handler(overflow_data: *UbsanTypes.OverflowData, lhs: UbsanTypes.ValueHandle, rhs: UbsanTypes.ValueHandle) callconv(.C) void {
            handleOverflow(overflow_data, name, symbol, lhs, rhs);
        }
    };

    exportHandlers(handlers, export_name);
}
