const std = @import("std");

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const ubsan_rt = b.addStaticLibrary(.{
        .name = "ubsan_rt",
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });
    ubsan_rt.linkLibC();
    ubsan_rt.linkLibCpp();

    b.installArtifact(ubsan_rt);
}
